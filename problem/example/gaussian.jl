using tn_julia

function gaussian(x)
    return exp(-x^2)
end
