using Interpolations
using LinearAlgebra
using Printf

function getAcont(odisc,Adisc,sigmab,g;emin=1e-12,emax=1e4,estep=200,tol=1e-14)
    # < Description >
    #
    # ocont,Acont = getAcont (odisc,Adisc,sigmab,g [,option])
    #
    # Broadening discrete spectral data of correlation functions given by
    # getAdisc. Here we apply two broadenings (i.e., convolutions) over all
    # frequencies sequentially.
    # The primary broadening is logarithmic broadening. It uses logarithmic
    # Gaussian or Gaussian kernel, where the broadening width (in linear
    # frequency scale) is proportional to the frequency bin position. For a
    # discrete weight at frequency bin of w', the value of kernel at w is given
    # by the symmetric logarithmic Gaussian,
    #
    #   L(w,w') = exp(-(log(w'/w)/sigmab - sigmab/4)^2)/(sqrt(pi)*sigmab*abs(w')),
    #                 for sign(w) == sign(w')),
    #   L(w,w') = 0,  otherwise.
    #
    # Here sigmab is an input parameter.
    # Then, appply the secondary linear broadening of width g to remove the
    # artifacts at low frequency. For the secondary broadening kernel, we use
    # the derivative of Fermi-Dirac function,
    #
    #   f(w,w') = 1/(1+cosh((w - w')/g))*(1/2/g)
    #           = - d(Fermi-Dirac function of temperature g)/d(energy)
    #
    # For detail, refer to [S.-S. B. Lee and A. Weichselbaum, Phys. Rev. B 94,
    # 235127 (2016)].
    #
    # < Input >
    # odisc : [numeric vector] Logarithimic frequency bins. Here the original
    #         frequency values from the differences b/w energy eigenvalues are
    #         shifted to the closest bins.
    # Adisc : [numeric vector] Spectral function.
    # sigmab : [numeric vector] The width parameter for the primary broadening
    #       kernel.
    # g : [numeric] The width parameter for secondary linear broadening kernel.
    #
    # < Option >
    # 'emin', .. : [numeric] Minimum absolute value of frequency grid. Set this
    #       be equal to or smaller than the minimum of finite elements of
    #       odisc, to properly broaden the spectral weights at frequencies
    #       lower than 'emin'. The spectral weights binned at frequencies
    #       smaller that emin are *not* broadened by the primary logarithmic
    #       broadening; they are broadened only by the secondary linear
    #       broadening.
    #       (Default : 1e-12)
    # 'emax', .. : [numeric] Maximum absoulte value of frequency grid.
    #       (Default : 1e4)
    # 'estep', .. : [integer] Number of frequency grid points per decade, i.e.,
    #       between a frequency and the frequency times 10 (e.g., between 1 and
    #       10).
    #       (Default: 200)
    # 'tol', .. : [numeric] Minimum value of (spectral weight)*(value of
    #       broadening kernel) to consider. The spectral weights whose
    #       contribution to the curve Acont are smaller than tol are not
    #       considered. Also the far-away tails of the broadening kernel, whose
    #       resulting contribution to the curve are smaller than tol, are not
    #       considered also.
    #       (Default : 1e-14)
    # '-v' : Show details.
    #
    # < Output >
    # ocont : [numeric vector] Logarithimic frequency grid.
    # Acont : [numeric vector] Smoothened spectral function, on the frequency
    #       grid 'ocont'.
    #
    # Written by S.Lee (May 22,2017)
    # Rewritten by S.Lee (Jun.20,2020)
    
    # temporary frequency grid; includes 0 in the middle
    xs = (floor(log10(emin)*estep):ceil(log10(emax)*estep))/estep; # log-frequencies; increasing, column vector
    ocont = 10 .^xs
    ocont = vcat(reverse(-ocont,dims=1),[0],ocont); # center of frequency bins
    docs = vcat(ocont[2]-ocont[1],(ocont[3:end]-ocont[1:end-2])/2,ocont[end]-ocont[end-1]); # width of frequency bins
    Acont = zeros(length(ocont),1); # temporary result
    
    oks1 = (odisc .>= emin)
    if any(oks1)
        odtmp = odisc[oks1]
        Adtmp = Adisc[oks1,:]
        ots,dots,yts = getAcont_logBroaden(odtmp,Adtmp,sigmab,tol,emin,emax,estep)
        Acont = getAcont_linBroaden(ots,dots,yts,g,ocont,docs,Acont,tol)
    end
    
    oks2 = (odisc .<= -emin)
    if any(oks2)
        odtmp = -odisc[oks2]; # negative -> positive
        Adtmp = Adisc[oks2,:]
        ots,dots,yts = getAcont_logBroaden(odtmp,Adtmp,sigmab,tol,emin,emax,estep)
        Acont = getAcont_linBroaden(-ots,dots,yts,g,ocont,docs,Acont,tol); # -ots: return to negative frequency
    end
    
    oks3 = .!any(hcat(oks1,oks2),dims=2)
    Acont = getAcont_linBroaden([0],[docs[Int((length(docs)+1)/2)]],[sum(sum(Adisc[oks3,:]))],g,ocont,docs,Acont,tol)
    
    return ocont,Acont
    
    end
    