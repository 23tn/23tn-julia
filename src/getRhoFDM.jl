using Interpolations
using LinearAlgebra
using Printf

function getRhoFDM(Inrg,T)
# < Description >
#
# Inrg = getRhoFDM(Inrg,T)
#
# Construct the full density matrix (FDM) in the basis of both discarded
# and kept states, for given temperature T.
#
# < Input >
# Inrg : [struct] NRG information obtained after running NRG_IterDiag.
# T : [number] Temperature. Here, we set \hbar = k_B = 1.
#
# < Ouput >
# Inrg : [struct] NRG result. It keeps the result of NRG_IterDiag. In
#       addition to the result, this function adds two more fields to Inrg:
#   .RD, .RK : [Any Array] Full density matrix in the discarded and kept state
#       basis, respectively. Each Any array element Inrg.RD[n] is a column
#       vector whose elements are the density matrix elements associated
#       with the discarded energy eigenenstates at the iteration n-1. (Note
#       that s00 for n = 1 is for the iteration diagmonalizing K00 basis.)
#       Inrg.RK[n] is a matrix in the basis of the kept energy eigenenstates
#       at the iteration n-1.

L = length(Inrg.E0)

# extract the local space dimension from ket tensors
locdim = zeros(L,1)
for itL = (1:L)
    if !isempty(Inrg.AK[itL])
        locdim[itL] = size(Inrg.AK[itL],2)
    else
        locdim[itL] = size(Inrg.AD[itL],2)
    end
end

# the shift of energy in each shell measured from the lowest-energy of the
# last iteration
E0r = vcat(Inrg.EScale[2:end].*Inrg.E0[2:end],[0])
E0r = reverse(cumsum(reverse(E0r,dims=1)),dims=1)

RD = Array{Any}(undef,1,L); # FDM in the discarded state basis; row vector
RK = Array{Any}(undef,1,L); # FDM in the kept state basis; matrix
RK[L] = [];

RDsum = zeros(1,L); # sum of Boltzmann weigenhts

# obtain the Boltzamann weigenhts
for itL = (1:L)
    # Obtain the column vector RD[itL] whose elements are the Boltzmann
    # weigenhts
    if isempty(Inrg.ED[itL])
        RD[itL] = []
        RDsum[itL] = 0
    else
        RD[itL] = exp.(-(Inrg.ED[itL]*Inrg.EScale[itL].-E0r[itL])/T)*prod(locdim[itL+1:end])
        RDsum[itL] = sum(RD[itL])
    end
end

RDsum = sum(RDsum)

# normalize the Boltzmann weigenhts to get the elements of the density matrix
# in the discarded basis
for itL = (1:L)
    RD[itL] = RD[itL]/RDsum
end

# update the FDM in the kept basis
for itL = (L:-1:2)
    # Construct RK[itL-1] as the sum of RD[itL] & RK[itL], with the local
    # Hilbert space for the site s[itL-1]. (Note that s00 for itL = 1, s01
    # for itL = 2, etc.)
    # Hint: one may utilize updateLeft; after permuting the legs of
    # Inrg.AD[itL] & Inrg.AK[itL].
    
    # to utilize updateLeft for contracting AD with RD; & AK with RK
    # permute the legs of AD & AK [left <-> right]
    AD2 = permutedims(Inrg.AD[itL],(3,2,1))
    AK2 = permutedims(Inrg.AK[itL],(3,2,1))
    if isempty(RD[itL]); diagRD = []; else; diagRD = diagm(RD[itL]); end;
    RK[itL-1] = updateLeft(diagRD,2,AD2,[],[],AD2
        ) + updateLeft(RK[itL],2,AK2,[],[],AK2)
    # NOTE: AK & AD are in left-canonical form; not right-canonical.
end

Inrg.RK = RK;
Inrg.RD = RD;

return Inrg

end
