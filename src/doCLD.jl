using Interpolations
using LinearAlgebra
using Printf

function doCLD(ozin,RhoV2in,Lambda,N;estep=10,emax=Inf,emin=1e-16)
# < Description >
#
# ff,gg = doCLD(ozin,RhoV2in,Lambda,N [; option])
#
# This function performs the Campo--Oliveira logarithmic discretization [V.
# L. Campo & L. N. Oliveira, Phys. Rev. B 72, 104432 (2005)] of the input
# hybridization function (paramterized by the inputs 'ozin' & 'RhoV2in')
# and maps the resulting star-geometry Hamiltonian onto the chain-geometry
# Hamiltonian, so-called the Wilson chain. The output 'ff' & 'gg'
# describe the hopping amplitudes and the on-site energies of the chain.
#
# < Input >
# ozin, RhoV2in : [numeric vector] The values of definining the hybridization
#       function \Gamma(\omega). RhoV2in[n] is the value of the
#       hybridization function at frequency ozin[n]. The first & last
#       elements of ozin define the bandwidth of the bath.
# N : [integer] Wilson chain length.
# Lambda : [numeric] Discretization parameter.
#
# < Option >
# estep = ... : [numeric] Number of step for resolving frequencies for
#       each discretization interval.
#       (Default: 10)
# emax = ... : [numeric] Maximum frequency value in defining the
#       hybridization function.
#       (Default: max(abs(ozin)), do this in function body)
# emin = ... : [numeric] Minimum frequency value in defining the
#       hybridization function.
#       (Default: 100*eps, do this in function body)
#
# < Output >
# ff, gg : [numeric vectors] Hopping amplitudes and on-site energies of
#       the Wilson chain; respectively. The hopping amplitudes correspond
#       to the superdiagonals [diagm(1 => ..) & diagm(-1 => ..)] of the
#       tridiagonal matrix representation of a single-particle Hamiltonian;
#       the on-site energies correspond to the diagonals of the tridiagonal
#       matrix.

# default parameter
estep = 10;
emax = maximum(abs.(ozin));
emin = 1e-5*eps(Float64);

# logarithmic grid
xs = ((log(emax)/log(Lambda)*estep):-1:(log(emin)/log(Lambda)*estep))/estep
xs = reverse(xs,dims=1); # increasing, column
oz = Lambda.^xs; # increasing

interp = LinearInterpolation(ozin,RhoV2in,extrapolation_bc=Line())

rho1 = interp(+oz)
rho1[rho1.<0] .= 0; # to ensure positivity
repE1,repT1 = doCLD_1side(oz,rho1,estep)

rho2 = interp(-oz)
rho2[rho2.<0] .= 0; # to ensure positivity
repE2,repT2 = doCLD_1side(oz,rho2,estep)

if (length(repE1)+length(repE2)) < N
    @printf("WRN: Number of discretization intervals is smaller than the chain length")
    N2 = length(repE1) + length(repE2)
else
    N2 = N
end

ff = zeros(N2); # hopping amplutudes; corresponds to the super diagmonal 
                  # (diagm(..,+1) | diagm(..,-1)) in the tridiagmonal matrix
gg = zeros(N2); # hopping amplutudes; corresponds to the main diagmonal 
                  # (diagm(..)) in the tridiagmonal matrix

# # Lanczos tridiagmonalization
# star-geometry Hamiltonian
Xis = vcat(reverse(repE1,dims=1),0,-repE2)
Gammas = vcat(reverse(sqrt.(repT1),dims=1),0,sqrt.(repT2))
H = diagm(Xis)
id = length(repE1)+1
H[:,id] = Gammas
H[id,:] = Gammas'

U = zeros(size(H,1),1)
U[id,1] = 1

for itN = (1:N2)
    v = H*U[:,itN]
    v = v-U*(U'*v)
    v = v-U*(U'*v); # twice for numerical reason
    ff[itN] = norm(v)

    if (itN < N2) && (ff[itN] > 0)
        U = hcat(U,v/ff[itN])
        gg[itN] = U[:,itN+1]'*H*U[:,itN+1]
    end
end

return ff,gg

end


function doCLD_1side(oz,rho,nstep)
# Obtain the representative energies [repE, \mathcal[E] in Campo & Oliveira] &
# the integral of the hybridization function (repT) for each discretization
# interval; for either positive | negative energy side.

ids = (length(rho):-nstep:1); # index of oz & rho at the points of the discretization grids

repT = zeros(length(ids)-1)
repE = zeros(size(repT))

for itx = (1:length(repT))
    # compute the integrals for each interval
    ozp = oz[ids[itx+1]:ids[itx]]
    rhop = rho[ids[itx+1]:ids[itx]]
    repT[itx] = sum((rhop[2:end]+rhop[1:end-1]).*(ozp[2:end]-ozp[1:end-1]))/2
    repE[itx] = (rhop[end]-rhop[1]) +
            sum((ozp[2:end].*rhop[1:end-1] - ozp[1:end-1].*rhop[2:end]) ./
                (ozp[2:end] - ozp[1:end-1]) .* log.(abs.(ozp[2:end]./ozp[1:end-1])))
end

repE = repT./repE

return repE,repT

end
