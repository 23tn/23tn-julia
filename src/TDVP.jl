function TDVP_1site(M,Hs,O,Nkeep,dt)
# < Description >
#
# M,Ovals = TDVP_1site(M,Hs,O,Nkeep,dt)
#
# Time-dependent variational principle (TDVP) method for simulating
# real-time evolution of matrix product state (MPS). The expectation
# values of local operator O for individual sites are evaluated for
# discrete time instances. The 1D chain system is described by the matrix
# product operator (MPO) Hamiltonian Hs.
#
# < Input >
# M : [Any Array] The initial state as the MPS. The length of M, length(M),
#       defines the chain length. The leg convention of M[n] is as follows:
#
#    1      3   1      3         1        3
#   ---M[1]---*---M[2]---* ... *---M[end]---
#       |          |                 |
#       ^2         ^2                ^2
#
# Hs : [Any Array] MPO description of the Hamiltonian. Each Hs[n] acts on 
#       site n & is a rank-4 tensor. The order of legs of Hs[n] is left-
#       bottom-right-top, where bottom [top] leg is to be contracted with
#       bra (ket) tensor:
#
#       |4          |4
#    1  |   3    1  |   3
#   ---Hs[1]---*---Hs[2]---*--- ...
#       |           |
#       |2          |2
#
# O : [matrix] Rank-2 tensor as a local operator acting on a site. The
#       expectation value of this operator at each chain site is to be
#       computed; see the description of the output "Ovals" for detail.
# Nkeep : [integer] Maximum bond dimension.
# dt : [numeric] Real time step size. Each real-time evolution by step dt
#       consists of one pair of sweeps [left-to-right & right-to-left].
#
# < Output >
# M : [Any Array] The final MPS after real-time evolution.
# Ovals : [matrix] Ovals[1,n] indicates the expectation value of local 
#       operator O (input) at the site n after the time step dt (input).

N = length(M);

# results
Ovals = convert(Matrix{ComplexF64},zeros(1,N))

# # Hamiltonian for the left/right parts of the chain
Hlr = Array{Any}(undef,1,N+2)
Hlr[1] = reshape([1],(1,1,1));
Hlr[end] = reshape([1],(1,1,1));
# Hlr[1] & Hlr[end] are dummies; they will be kept empty. These dummies
# are introduced for convenience.

# Since M is in right-canonical form by now, Hlr[..] are the right parts of
# the Hamiltonian. That is, Hlr[n+1] is the right part of Hamiltonian which()
# is obtained by contracting M[n:end] with Hs[n:end]. (Note the index for
# Hlr is n+1, not n, since Hlr[1] is dummy.)
for itN = (N:-1:1)
    T = permutedims(M[itN],(3,2,1)); # permute left<->right, to make use of updateLeft()
    if itN == N
        # "remove" the right leg [3rd leg] of Hs[itN]
        H2 = permutedims(Hs[itN],(2,1,4,3)); # bottom-left-top [-right]
        H2 = H2[:,:,:,1]
        Hlr[itN+1] = updateLeft([],[],T,H2,3,T)
    elseif itN == 1
        Hlr[itN+1] = reshape([1],(1,1,1))
    else
        # permute left<->right; to make use of updateLeft
        H2 = permutedims(Hs[itN],(3,2,1,4)); # right-bottom-left-top
        Hlr[itN+1] = updateLeft(Hlr[itN+2],3,T,H2,4,T)
    end
end

# left -> right
for itN = (1:(N-1))
    # time evolution of site-canonical tensor ["A tensor"] M[itN], via
    # TDVP_1site_expHA
    
    Anew = TDVP_1site_expHA(Hlr[itN],Hs[itN],Hlr[itN+2],M[itN],dt/2)
    
    # update M[itN] & generate Cold by using Anew, via SVD
    M[itN],S2,V2 = 
        svdTr(Anew,3,[1,2],Nkeep,-1); # set Stol as -1, not to truncate even zero singular values
    Cold = contract(diagm(S2),2,2,V2,2,1)
    
    # update Hlr[itN+1] in effective basis
    if itN == 1
        # "remove" the left leg [1st leg] of Hs[itN]
        H2 = permutedims(Hs[itN],(2,3,4,1)); # bottom-right-top [-left]
        H2 = H2[:,:,:,1]
        Hlr[itN+1] = updateLeft([],[],M[itN],H2,3,M[itN])
    else
        Hlr[itN+1] = updateLeft(Hlr[itN],3,M[itN],Hs[itN],4,M[itN])
    end
    
    # inverse time evolution of C tensor [Cold -> Cnew], via TDVP_1site_expHC
    Cnew = TDVP_1site_expHC(Hlr[itN+1],Hlr[itN+2],Cold,dt/2)
    
    # absorb Cnew into M[itN+1]
    M[itN+1] = contract(Cnew,2,2,M[itN+1],3,1)
end

itN = N; # right end
M[itN] = TDVP_1site_expHA(Hlr[itN],Hs[itN],Hlr[itN+2],M[itN],dt)

# right -> left
for itN = ((N-1):-1:1)
    # update M[itN+1] & generate Cold via SVD
    U2,S2,M[itN+1] = 
        svdTr(M[itN+1],3,1,Nkeep,-1); # set Stol as -1, not to truncate even zero singular values
    Cold = contract(U2,2,2,diagm(S2),2,1)
    
    # update Hlr[itN+2] in effective basis
    T = permutedims(M[itN+1],(3,2,1)); # permute left<->right, to make use of updateLeft()
    if (itN+1) == N
        # "remove" the right leg [3rd leg] of Hs[N]
        H2 = permutedims(Hs[itN+1],(2,1,4,3)); # bottom-left-top [-right]
        H2 = H2[:,:,:,1]
        Hlr[itN+2] = updateLeft([],[],T,H2,3,T)
    else
        # permute left<->right; to make use of updateLeft
        H2 = permutedims(Hs[itN+1],(3,2,1,4)); # right-bottom-left-top
        Hlr[itN+2] = updateLeft(Hlr[itN+3],3,T,H2,4,T)
    end
    
    # inverse time evolution of C tensor [Cold -> Cnew], via TDVP_1site_expHC
    Cnew = TDVP_1site_expHC(Hlr[itN+1],Hlr[itN+2],Cold,dt/2)
    
    # absorb Cnew into M[itN]
    M[itN] = contract(M[itN],3,3,Cnew,2,1)
    
    # time evolution of site-canonical tensor ["A tensor"] M[itN], via TDVP_1site_expHA
    M[itN] = TDVP_1site_expHA(Hlr[itN],Hs[itN],Hlr[itN+2],M[itN],dt/2)
end

# Measurement of local operators O; currently M is in site-canonical
# with respect to site 1
MM = reshape([1],(1,1)); # contraction of bra/ket tensors from left
for itN = (1:N)
    Ovals[1,itN] = tr(updateLeft(MM,2,M[itN],O,2,M[itN]))
    MM = updateLeft(MM,2,M[itN],[],[],M[itN])
end

return M,Ovals

end



function TDVP_1site_expHA(Hleft,Hloc,Hright,Aold,dt)
# Time evolution for "A tensor" Aold by time step dt; by using Hlr tensors
# that are the Hamiltonian in effective basis. Anew is the result after
# time evolution.
# This subfunction is adapted from the Lanczos routine "DMRG/eigs_1site.jl".
# The difference from "eigs_1site" is that it considers the time evolution
# not the ground state; & does not consider the orthonormal state.

# default parameters
N = 5
minH = 1e-10

Asz = (size(Aold,1),size(Aold,2),size(Aold,3)); # size of ket tensor
Akr = convert(Matrix{ComplexF64},
      zeros(length(Aold),N+1)); # Krylov vectors [vectorized tensors]
Akr[:,1] = Aold[:]/norm(Aold[:]); # normalize Aold

# In the Krylov basis; the Hamiltonian becomes tridiagmonal
ff = convert(Vector{ComplexF64},zeros(N)); # 1st diagmonal
gg = convert(Vector{ComplexF64},zeros(N+1)); # main diagmonal

for itN = (1:(N+1))
    # contract Hamiltonian with ket tensor
    Atmp = TDVP_1site_HA(Hleft,Hloc,Hright,reshape(Akr[:,itN],Asz))
    Atmp = Atmp[:]; # vectorize
    
    gg[itN] = Akr[:,itN]'*Atmp; # diagmonal element; "on-site energy"
    
    if itN < (N+1)
        # orthogonalize Atmp w.r.t. the previous ket tensors
        Atmp = Atmp - Akr[:,(1:itN)]*(Akr[:,(1:itN)]'*Atmp)
        Atmp = Atmp - Akr[:,(1:itN)]*(Akr[:,(1:itN)]'*Atmp); # twice, to reduce numerical noise
        
        # norm
        ff[itN] = norm(Atmp)
        
        if real(ff[itN]) > minH
            Akr[:,itN+1] = Atmp/ff[itN]
        else
            # stop iteration; truncate ff; gg
            ff = ff[1:itN-1]
            gg = gg[1:itN]
            Akr = Akr[:,1:itN]
            break
        end
    end
    
end

# Hamiltonian in the Krylov basis
Hkr = diagm(1 => ff)
Hkr = Hkr + Hkr' + diagm(gg)
Ekr,Vkr = eigen((Hkr+Hkr')/2)

Anew = Akr*(Vkr*(diagm(exp.((-1im*dt)*Ekr))*Vkr[1,:]))
Anew = Anew/norm(Anew); # normalize
Anew = reshape(Anew,Asz); # reshape back to rank-3 tensor

return Anew

end


function TDVP_1site_HA(Hleft,Hloc,Hright,Ain)
# Adapted from the subfunction "eigs_1site_HA' in 'DMRG/eigs_1site.jl".

# set empty tensors as 1; for convenience
if isempty(Hleft)
    Hleft = reshape([1],(1,1,1))
end
if isempty(Hright)
    Hright = reshape([1],(1,1,1))
end

Aout = contract(Hleft,3,3,Ain,3,1)
Aout = contract(Aout,4,[2,3],Hloc,4,[1,4])
Aout = contract(Aout,4,[2,4],Hright,3,[3,2])

return Aout
    
end



function TDVP_1site_expHC(Hleft,Hright,Aold,dt)
# Time evolution for "C tensor" Aold by time step -dt; by using Hlr tensors
# that are the Hamiltonian in effective basis. Cnew is the result after
# time evolution.
# This subfunction is adapted from the subfunction "TDVP_1site_expHA"
# above. The differences here are that the ket tensor is rank-2; that there
# is no the local Hamiltonian "Hloc"; & that the time evolution is in an
# inverse direction.

# default parameters
N = 5
minH = 1e-10

Asz = (size(Aold,1),size(Aold,2)); # size of ket tensor
Akr = convert(Matrix{ComplexF64},
      zeros(length(Aold),N+1)); # Krylov vectors [vectorized tensors]
Akr[:,1] = Aold[:]/norm(Aold[:]); # normalize Aold

# In the Krylov basis; the Hamiltonian becomes tridiagmonal
ff = convert(Vector{ComplexF64},zeros(N)); # 1st diagmonal
gg = convert(Vector{ComplexF64},zeros(N+1)); # main diagmonal

for itN = (1:(N+1))
    
    # contract Hamiltonian with ket tensor
    Atmp = TDVP_1site_HC(Hleft,Hright,reshape(Akr[:,itN],Asz))
    Atmp = Atmp[:]; # vectorize
    
    gg[itN] = Akr[:,itN]'*Atmp; # diagmonal element; "on-site energy"
    
    if itN < (N+1)
        # orthogonalize Atmp w.r.t. the previous ket tensors
        Atmp = Atmp - Akr[:,(1:itN)]*(Akr[:,(1:itN)]'*Atmp)
        Atmp = Atmp - Akr[:,(1:itN)]*(Akr[:,(1:itN)]'*Atmp); # twice, to reduce numerical noise
        
        # norm
        ff[itN] = norm(Atmp)
        
        if real(ff[itN]) > minH
            Akr[:,itN+1] = Atmp/ff[itN]
        else
            # stop iteration; truncate ff; gg
            ff = ff[1:itN-1]
            gg = gg[1:itN]
            Akr = Akr[:,1:itN]
            break
        end
    end
end

# Hamiltonian in the Krylov basis
Hkr = diagm(1 => ff)
Hkr = Hkr + Hkr' + diagm(gg)
Ekr,Vkr = eigen((Hkr+Hkr')/2)

Anew = Akr*(Vkr*(diagm(exp.((+1im*dt)*Ekr))*Vkr[1,:])); # inverse
Anew = Anew/norm(Anew); # normalize
Anew = reshape(Anew,Asz); # reshape back to rank-2 tensor

return Anew

end


function TDVP_1site_HC(Hleft,Hright,Ain)
# Adapted from the subfunction "TDVP_1site_HA" above.

# set empty tensors as 1; for convenience
if isempty(Hleft)
    Hleft = reshape([1],(1,1,1))
end
if isempty(Hright)
    Hright = reshape([1],(1,1,1))
end

Aout = contract(Hleft,3,3,Ain,2,1)
Aout = contract(Aout,3,[2,3],Hright,3,[2,3])

return Aout
    
end