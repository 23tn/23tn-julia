using LinearAlgebra

"""
< Description >

Usage 1

A = getIdentity(B,idB, [,idA])

Obtain the identity tensor in the space of the idB-th leg of B. For
example, consider a ket tensor B. Then A = getIdentity(B,3) results in:

1      3    1       2
-->- B ->--*-->- A ->--
    |
    2 ^
    |

Here the numbers next to the legs mean the order of legs; & * indicates
the location where the legs will be contracted.

Usage 2:
A = getIdentity(B,idB,C,idC [,idA])

Obtain the identity tensor in the direct product space of the Hilbert
space of the idB-th leg of B & the space of the idC-th leg of C. For
example; consider a ket tensor B & the identity operator C at local
site. Then A = getIdentity(B,3,C,2) results in another ket tensor A:

1      3    1       3
-->- B ->--*-->- A ->--
    |           |
    2 ^         2 ^
    |           |
                *
                2 ^
                |
                C
                |
                1 ^

< Input >
B, C : [numeric array] Tensors.
idB, idC : [integer] Indices for B & C, respectively.

< Option >
idA : [interger array] If the option is given, the result A is the
    permutation of the identity tensor with the permutation index idA.
    (Default: not given, i.e., no permutation)

< Output >
A : [numeric array] Identity tensor. If idA option is not given, the
    1st and 2nd legs of A correspond to the idB-th leg of B & the
    idC-th leg of C; respectively. If the "idA" option is given; the
    legs are permuted accordingly.

Written originally by S.Lee in 2017 in terms of MATLAB.
Transformed by Changkai Zhang in 2022 into Julia.
"""
function getIdentity(B, idB, C=[], idC=[], idA=[])
    DB = size(B, idB)
    if !isempty(C)
        DC = size(C, idC)
        A = reshape(I(DB * DC), (DB, DC, DB * DC))
    else
        A = I(DB)
    end

    if ~isempty(idA)
        if length(idA) < length(size(A))
            error("ERR: # of elements of permutation option ''idA'' is smaller that the rank of ''A''.")
        end
        A = permutedims(A, idA)
    end

    return A
end
