using Interpolations
using LinearAlgebra
using Printf

mutable struct InrgData
    Lambda
    EScale
    EK
    AK
    ED
    AD
    E0
    RD
    RK
    InrgData(Lambda=undef,EScale=undef,EK=undef,AK=undef,RD=undef,RK=undef,
             ED=undef,AD=undef,E0=undef) = new(Lambda,EScale,EK,AK,RD,RK,ED,AD,E0)
end

function NRG_IterDiag(H0,A0,Lambda,ff,F,gg,NF,Z,Nkeep)
# < Description >
#
# Inrg = NRG_IterDiag (H0,A0,Lambda,ff,F,gg,NF,Z,Nkeep)
#
# Iterative diagonalization of the numerical renormalization group (NRG)
# method. Here the first chain site, associated with the second leg of A0,
# is for the impurity. The second chain site, which is to be included
# along the iterative diagonalization, is the first bath site. The hopping
# between the first and second sites is given by ff[1].
#
# This NRG style of the iterative diagonalization differs from the
# iterative diagonalization covered in earlier tutorial in that (i) the
# Hamiltonian is rescaled by the energy scale factors [see the output
# Irng.EScale below for detail], and (ii) the energy eigenenvalues are
# shifted so that the lowest energy eigenenvalue becomes zero.
#
#
# < Input >
# H0 : [rank-2 tensor] Impurity Hamiltonian which acts on the space of the
#       third (i.e., right) leg of A0. 
# A0 : [rank-3 tensor] Isometry for the impurity. The first (i.e., left)
#       and second (i.e., bottom) legs span the local spaces, and the third
#       (i.e., right) leg spans the full impurity Hilbert space.
# Lambda : [number] Logarithmic discretization parameter.
# ff : [vector] Hopping amplitudes of the Wilson chain. ff[1] is the
#       hopping between the impurity and the first bath site; ff[2] is
#       the hopping between the first and second bath sites, etc.
# F : [rank-3 tensor] Fermion annihilation operator.
# gg : [vector] On-site energies of the Wilson chain. gg[1] is the
#       on-site energy of the first bath site; gg[2] is the on-site
#       energy of the second bath site, etc.
# NF : [rank-2 tensor] Particle number operator associated with the on-site
#       energy.
# Z : [rank-2 tensor] Fermion anti-commutation sign operator.
# Nkeep : [number] Number of states to be kept. To have better separation
#       of the low-lying kept states and the high-lying discarded states,
#       the truncation threshold is set at the mostly separated states,
#       among the states starting from the Nkeep-th lowest-lying state to
#       the (Nkeep*1.1)-th lowest-lying state. The factor 1.1 is controlled
#       by a variable 'Nfac' defined below.
#
# < Output >
# Inrg : [struct] NRG result.
#   .Lambda : [number] Given by input.
#   .EScale : [vector] Energy scale to rescale the exponentially decaying
#             energy scale. It rescales the last hopping amplitude to be 1.
#   .EK : [Any array] Column vector of kept energy eigenenvalues. These
#             energy eigenenvalues are rescaled by Inrg.EScale and shifted by
#             Inrg.E0. As the result of shifting, the smallest value of EK
#             is zero.
#   .AK : [Any array] Rank-3 tensor for kept energy eigenenstates.
#   .ED : [Any array] Column vector of discarded energy eigenenvalues. These
#             energy eigenenvalues are rescaled by Inrg.EScale and shifted by
#             Inrg.E0.
#   .AD : [Any array] Rank-3 tensor for discarded energy eigenenstates.
#   .E0 : [vector] Ground-state energy at every iteration. Inrg.E0[n] is in
#             the unit of the energy scale given by Inrg.EScale[n].
#   The n-th elements, EScale[n], EK[n], AK[n], ED[n], AD[n], and E0[n],
#   are associated with the same iteration n. At iteration n, the part of
#   the chain which consists of the impurity and n-1 bath sites is
#   considered.

Nfac = 0.1; # up to 10# more states can be kept

# # error checking
if length(size(H0)) != 2
    error("ERR: H0 should be of rank 2.")
end
if length(size(A0)) != 3
    error("ERR: A0 should be of rank 3.")
end

Inrg = InrgData()
Inrg.Lambda = Lambda
N = length(ff)+1; # Length of the Wilson chain

# Rescaling factor (to divide the energy values):
# EScale[1] = 1 [no rescaling for the impurity], EScale[end] rescales
# ff[end] to be 1.
Inrg.EScale = vcat([1],(Lambda.^(((N-2):-1:0)/2))*ff[end])

Inrg.EK = Array{Any}(undef,1,N); 
Inrg.AK = Array{Any}(undef,1,N); 
Inrg.ED = Array{Any}(undef,1,N); 
Inrg.AD = Array{Any}(undef,1,N); 
Inrg.E0 = zeros(1,N);

for itN = (1:N)
    global Hprev, Fprev
    if itN == 1 # impurity only
        Inrg.AK[itN] = A0; # don't rotate the basis only for the first iteration
        eigenH0,_ = eigen(H0);
        Inrg.EK[itN] = sort(eigenH0); # but compute energy eigenenvalues for later analyses
        Inrg.AD[itN] = zeros(size(A0,1),size(A0,2),0); # no discarded states
        Inrg.ED[itN] = zeros(0,1); 
        
        # to be used in the next iteration
        Hprev = H0
    else # including bath sites
        Anow = getIdentity(Hprev,2,Z,2)
        
        # Hamiltonian from the previous iteration; expand to the englarged
        # Hilbert space
        Hnow = updateLeft(Hprev,2,Anow,[],[],Anow); 
        Hnow = Hnow*(Inrg.EScale[itN-1]/Inrg.EScale[itN]); # rescaling
        
        Fnow = permutedims(conj(F),(3,2,1)) # creation operator at site s[itN-1]
        Fnow = contract(Fnow,3,3,Z,2,1); # F'*Z; contract fermionic sign operator
        
        # hopping from the currently added site to the site added at the
        # last iteration
        Hhop = updateLeft(Fprev,3,Anow,Fnow,3,Anow)
        Hhop = (ff[itN-1]/Inrg.EScale[itN])*Hhop; # multiply rescaled hopping amplitude
        Hhop = Hhop+Hhop'; # add the hopping the last site to the current site
        
        Hon = updateLeft([],[],Anow,NF,2,Anow); # on-site term
        Hon = (gg[itN-1]/Inrg.EScale[itN])*Hon; # multiply rescaled on-site energy
        
        Hnow = Hnow+Hhop+Hon
        
        # diagmonalize Hamiltonian
        D,V = eigen((Hnow+Hnow')/2)
        # sort eigenenvalues & eigenenvectors in the order of increasing
        # eigenenvalues
        ids = sortperm(D)
        D = D[ids]
        V = V[:,ids]
        Inrg.E0[itN] = D[1]; # the ground state energy at each iteration
        D = D .- Inrg.E0[itN]; # overal shift to make the lowest energy value be 0
        
        if itN < N
            if length(D) > Nkeep
                # find largest separation of energy eigenenvalues
                ids = Int.(Nkeep:min(length(D),ceil(Nkeep*(1+Nfac))))
                _,maxid = findmax(diff(D[ids]))
                Ntr = ids[maxid]
            else
                Ntr = length(D)
            end
        else
            # discard all the states at the last iteration; the reason will
            # be revealed by the concept of the complete basis
            # (Anders-Schiller basis).
            Ntr = 0
        end
        
        Inrg.EK[itN] = D[1:Ntr]
        Inrg.ED[itN] = D[(Ntr+1):end]
        Inrg.AK[itN] = contract(Anow,3,3,V[:,1:Ntr],2,1)
        Inrg.AD[itN] = contract(Anow,3,3,V[:,(Ntr+1):end],2,1)
        
        Hprev = diagm(Inrg.EK[itN])
    end
    
    # to generate future hopping term
    if itN < N
        Fprev = updateLeft([],[],Inrg.AK[itN],F,3,Inrg.AK[itN])
    end
    
    # information on truncation
    if isempty(Inrg.EK[itN])
        Etr1 = 0
    else
        Etr1 = Inrg.EK[itN][end]
    end
    Ntr1 = length(Inrg.EK[itN])
    if isempty(Inrg.ED[itN])
        Etr2 = Etr1
    else
        Etr2 = Inrg.ED[itN][end]
    end
    Ntr2 = Ntr1+length(Inrg.ED[itN])
    
    @printf("#%02i/%02i : NK=%i/%i, EK=%.4g/%.4g\n",
                itN-1,N-1,Ntr1,Ntr2,Etr1,Etr2);
end

return Inrg
    
end
