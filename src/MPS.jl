
function svdTr(T, rankT, idU, Nkeep, Stol)
    # < Description >
    #
    # U,S,Vd,dw = svdTr (T,rankT,idU,Nkeep,Stol) # truncate by Nkeep & Stol
    # U,S,Vd,dw = svdTr (T,rankT,idU,[],Stol) # truncate by Stol
    # U,S,Vd,dw = svdTr (T,rankT,idU,Nkeep,[]) # truncate by Nkeep
    # U,S,Vd,dw = svdTr (T,rankT,idU,[],[]) # not truncate [only the default tolerance Stol = 1e-8 is considered]
    #
    # Singular value decomposition of tensor such that T = U*diagm(S)*Vd. (Note
    # that it is not U*S*V' as in the Julia built-in function 'svd'.) If the
    # truncation criterion is given; the tensors are truncated with respect to
    # the largest singluar values.
    #
    # < Input >
    # T : [tensor] Tensor.
    # rankT : [number] Rank of T.
    # idU : [integer vector] Indices of T to be associated with U. For example
    #       if rankT == 4 & idU == [1,3], the result U is rank-3 tensor whose
    #       1st and 2nd legs correspond to the 1st & 3rd legs of T. The 3rd
    #       leg of U is associated with the 1st leg of diagm(S). And Vd is
    #       rank-3 tensor whose 2nd and 3rd legs correspond to the 2nd & 4th
    #       legs of T. Its 1st leg is associated with the 2nd leg of diag(S).
    # Nkeep : [number] The number of singular values to keep.
    #       (Default: Inf, i.e., no truncation)
    # Stol : [number] Minimum magnitude of the singluar value to keep.
    #       (Default: 1e-8, which is the square root of double precision 1e-16)
    #
    # < Output >
    # U : [tensor] Tensor describing the left singular vectors. Its last leg
    #       contracts with diagm(S). The earlier legs are specified by input
    #       idU; their order is determined by the ordering of idU.
    # S : [vector] The column vector of singular values.
    # Vd : [tensor] Tensor describing the right singular vectors. Its 1st leg
    #       contracts with diagm(S). The later legs conserve the order of the
    #       legs of input T.
    # dw : [tensor] Discarded weight (= sum of the square of the singular
    #       values truncated).
    #
    # Written originally by S.Lee in 2017 in terms of MATLAB.
    # Transformed by Changkai Zhang in 2022 into Julia.

    # default truncation parameters
    if isempty(Nkeep)
        Nkeep = Inf
    end
    if isempty(Stol)
        Stol = 1e-8
    end

    Tdim = size(T) # dimensions of tensors

    if rankT != length(Tdim)
        error("ERR: Input ''rankT'' different from the rank of other input ''T''.")
    end

    idTtot = (1:length(Tdim))

    idV = setdiff(idTtot, idU)
    # reshape to matrix form
    T2 = reshape(permutedims(T, tuple(cat(dims=1, idU, idV))...), (prod(Tdim[idU]), prod(Tdim[idV])))
    U2, S2, V2 = svd(T2) # SVD

    # number to be kept determined by Nkeep & by Stol
    Ntr = [Nkeep, sum(S2 .> Stol)]
    # choose stricter criterion; round up & compare with 1 just in case
    Ntr = max(convert(Int, ceil(minimum(Ntr))), 1) # keep at least one bond to maintain the tensor network structure

    dw = sum(S2[Ntr+1:end] .^ 2)

    S2 = S2[1:Ntr]
    U2 = U2[:, (1:Ntr)]
    V2 = V2[:, (1:Ntr)]

    U = reshape(U2, (Tdim[idU]..., Ntr))
    S = S2
    Vd = reshape(V2', (Ntr, Tdim[idV]...))

    return U, S, Vd, dw

end

function canonForm(M, id, Nkeep=Inf)
    # < Description >
    #
    # M,S,dw = canonForm (M,id [,Nkeep])
    #
    # Obtain the canonical forms of MPS. It brings the tensors M[1], ..., M[id]
    # into the left-canonical form & the others M[id+1], ..., M[end] into the
    # right-canonical form.
    #
    # < Input >
    # M : [Any array] MPS of length length(M). Each cell element is a rank-3
    #       tensor; where the first; second; & third dimensions are
    #       associated with left, bottom [i.e., local], & right legs
    #       respectively.
    # id : [integer] Index for the bond connecting the tensors M[id] &
    #       M[id+1]. With respect to the bond, the tensors to the left
    #       (right) are brought into the left-(right-)canonical form. If id ==
    #       0; the whole MPS will be in the right-canonical form.
    #
    # < Option >
    # Nkeep : [integer] Maximum bond dimension. That is, only Nkeep the
    #       singular values & their associated singular vectors are kept at
    #       each iteration.
    #       (Default: Inf)
    #
    # < Output >
    # M : [Any array] Left-, right-, | bond-canonical form from input M
    #       depending on id; as follows:
    #       * id == 0: right-canonical form
    #       * id == length(M): left-canonical form
    #       * otherwise: bond-canonical form
    # S : [column vector] Singular values at the bond between M[id] & M[id+1].
    # dw : [column vector] Vector of length length(M)-1. dw[n] means the
    #       discarded weight (i.e., the sum of the square of the singular
    #       values that are discarded) at the bond between M[n] & M[n+1].
    #
    # Written originally by S.Lee in 2019 in terms of MATLAB.
    # Transformed by Changkai Zhang in 2022 into Julia.

    dw = zeros(length(M) - 1, 1) # discarded weights

    # # Bring the left part of MPS into the left-canonical form
    for it = (1:id)

        # reshape M[it] & SVD
        T = M[it]
        T = reshape(T, (size(T, 1) * size(T, 2), size(T, 3)))
        U, S, V = svd(T)

        Svec = S # vector of singular values

        # truncate singular values/vectors; keep up to Nkeep. Truncation at the
        # bond between M[id] & M[id+1] is performed later.
        if ~isinf(Nkeep) && (it < id)
            nk = min(length(Svec), Nkeep) # actual number of singular values/vectors to keep
            dw[it] = dw[it] + sum(Svec[nk+1:end] .^ 2) # discarded weights
            U = U[:, (1:nk)]
            V = V[:, (1:nk)]
            Svec = Svec[1:nk]
        end

        S = diagm(Svec) # return to square matrix

        # reshape U into rank-3 tensor, & replace M[it] with it
        M[it] = reshape(U, (size(U, 1) ÷ size(M[it], 2), size(M[it], 2), size(U, 2)))

        if it < id
            # contract S & V' with M[it+1]
            M[it+1] = contract(S * V', 2, 2, M[it+1], 3, 1)
        else
            # R1: tensor which is the leftover after transforming the left
            #   part. It will be contracted with the counterpart R2 which is
            #   the leftover after transforming the right part. Then R1*R2 will
            #   be SVD-ed & its left/right singular vectors will be
            #   contracted with the neighbouring M-tensors.
            R1 = S * V'
        end

    end

    # # In case of fully right-canonical form; the above for-loop is not executed
    if id == 0
        R1 = 1
    end

    # # Bring the right part into the right-canonical form
    for it = (length(M):-1:id+1)

        # reshape M[it] & SVD
        T = M[it]
        T = reshape(T, (size(T, 1), size(T, 2) * size(T, 3)))
        U, S, V = svd(T)

        Svec = S # vector of singular values

        # truncate singular values/vectors; keep up to Nkeep. Truncation at the
        # bond between M[id] & M[id+1] is performed later.
        if ~isinf(Nkeep) && (it > (id + 1))
            nk = min(length(Svec), Nkeep) # actual number of singular values/vectors to keep
            dw[it-1] = dw[it-1] + sum(Svec[nk+1:end] .^ 2) # discarded weights
            U = U[:, (1:nk)]
            V = V[:, (1:nk)]
            Svec = Svec[1:nk]
        end

        S = diagm(Svec) # return to square matrix

        # reshape V' into rank-3 tensor, replace M[it] with it
        M[it] = reshape(V', (size(V, 2), size(M[it], 2), size(V, 1) ÷ size(M[it], 2)))

        if it > (id + 1)
            # contract U & S with M[it-1]
            M[it-1] = contract(M[it-1], 3, 3, U * S, 2, 1)
        else
            # R2: tensor which is the leftover after transforming the right
            #   part. See the description of R1 above.
            R2 = U * S
        end

    end

    # # In case of fully left-canonical form; the above for-loop is not executed
    if id == length(M)
        R2 = 1
    end

    # # SVD of R1*R2; & contract the left/right singular vectors to the tensors
    U, S, V = svd(R1 * R2)

    # truncate singular values/vectors; keep up to Nkeep. At the leftmost &
    # rightmost legs [dummy legs], there should be no truncation, since they
    # are already of size 1.
    if ~isinf(Nkeep) && (id > 0) && (id < length(M))

        nk = min(length(S), Nkeep) # actual number of singular values/vectors
        dw[id] = dw[id] + sum(S[nk+1:end] .^ 2) # discarded weights
        U = U[:, (1:nk)]
        V = V[:, (1:nk)]
        S = S[1:nk]

    end

    if id == 0 # fully right-canonical form
        # U is a single number which serves as the overall phase factor to the
        # total many-site state. So we can pass over U to V'.
        M[1] = contract(U * V', 2, 2, M[1], 3, 1)
    elseif id == length(M) # fully left-canonical form
        # V' is a single number which serves as the overall phase factor to the
        # total many-site state. So we can pass over V' to U.
        M[end] = contract(M[end], 3, 3, U * V', 2, 1)
    else
        M[id] = contract(M[id], 3, 3, U, 2, 1)
        M[id+1] = contract(V', 2, 2, M[id+1], 3, 1)
    end

    return M, S, dw

end
