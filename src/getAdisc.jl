using Interpolations
using LinearAlgebra
using Printf

function getAdisc(Inrg,Op,Z,emin=1e-12,emax=1e2,estep=200)
    # < Description >
    #
    # odisc,Adisc = getAdisc (Inrg,Op,Z [; option]); # for anti-commuting operators
    # odisc,Adisc = getAdisc (Inrg,Op,[] [; option]); # for commuting operators
    #
    # Obtain the discrete spectral function, given the operators at the
    # impurity (site s00) and the NRG information. Using the Lehmann
    # representation and the complete basis, we can obtain the spectral
    # function as a collection of delta functions with corresponding weigenhts. The
    # spectral weigenhts are binned in the vector Adisc, where each bin of Adisc
    # represents the narrow frequency interval centered at the corresponding
    # element of odisc.
    #
    # < Input >
    # Inrg : [struct] NRG information obtained after running NRG_IterDiag.
    # Op : [tensor] Operator acting at the impurity (site s00).
    # Z : [tensor] Fermionic sign operator. The sign \zeta is 1 if the input
    #		Z is not empty or -1 (fermionic) if Z is empty (bosonic).
    #		Thus, if the operator Op is bosonic, set Z as empty []. 
    #
    # < Option >
    # emin = .. : [number] Minimum (in absolute value) frequency limit.
    #               (Default: 1e-12)
    # emax = .. : [number] Maximum (in absolute value) frequency limit.
    #               (Default: 1e2)
    # estep = .. : [number] Number of bins per decade (frequency value 1 ->
    #               10).
    #               (Default: 200)
    #
    # < Output >
    # odisc : [vector] Logarithmic frequency grid.
    # Adisc : [vector] Discrete spectral weigenhts corresponding to odisc.
    
    if !isempty(Z)
        opsign = 1; # anti-commutation
    else
        opsign = -1; # commutation
    end
    
    logemin = log10(emin)
    logemax = log10(emax)
    
    odisc = 10 .^(((logemin*estep):(logemax*estep))/estep)
    Adisc = zeros(length(odisc),2); # column 1: negative freq., col 2: positive freq.
    
    if !isempty(Z)
        @printf("Correlation function for anti-commuting op.\n")
    else
        @printf("Correlation function for commuting op.\n")
    end
    
    # # Rerouting Z string: by doing this; we don't need to contract Z tensors
    # when we update Op for longer chains. Refer to the appendix of A.
    # Weichselbaum, Phys. Rev. B 86, 245124 [2012]. (Especially Figs. 10 &
    # 11)
    if !isempty(Z)
        Op = contract(Z,2,2,Op,3,1)
    end
    
    Oprev = updateLeft([],[],Inrg.AK[1],Op,3,Inrg.AK[1])
    N = length(Inrg.E0)
    
    for itN = (2:N)
        Atmp = [Inrg.AD[itN],Inrg.AK[itN]]
        Etmp = [Inrg.ED[itN],Inrg.EK[itN]]
        if isempty(Inrg.RD[itN]); 
            Rtmp = [[],Inrg.RK[itN]]
        else
            Rtmp = [diagm(Inrg.RD[itN]),Inrg.RK[itN]]
        end
        
        for it1 = (1:2) # D[iscarded], K[ept]
            for it2 = (1:2) # D, K
                if !isempty(Atmp[it1]) && !isempty(Atmp[it2])
                    # Onow: Operator in the current subspace()
                    Onow = updateLeft(Oprev,3,Atmp[it1],[],[],Atmp[it2])
                    # ROp; OpR: contraction of density matrix & Onow
                    ROp = contract(Rtmp[it1],2,2,Onow,3,1)
                    OpR = contract(Onow,3,3,Rtmp[it2],2,1)
    
                    if (it1 == 2) && (it2 == 2)
                        Oprev = Onow
                    else
                        # compute discrete spectral weigenhts & add to the
                        # result "Adisc"
                        Adisc = Adisc + getAdisc_1shell(Onow,ROp + opsign*OpR, 
                            Etmp[it1],Etmp[it2],Inrg.EScale[itN], 
                            length(odisc),logemin,estep)
                    end
                end
            end
        end
        
        # show accumulative sum of the discrete weigenhts
        @printf("# %02i/%02i, : sum(Adisc) = %.4g\n",itN-1,N-1,sum(Adisc)); 
    end
    
    odisc = odisc[:]
    odisc = vcat(reverse(-odisc,dims=1),odisc)
    Adisc = vcat(reverse(Adisc[:,1],dims=1),Adisc[:,2])
    
    return odisc,Adisc
    
    end
    
    
    function getAdisc_1shell(Op1,Op2,E1,E2,EScale,nodisc,logemin,estep)
    # compute spectral weigenhts for given operators Op1 & Op2.
    
    # permute the operator legs so that the operator-flavor leg is placed at
    # the last
    Op1 = permutedims(Op1,(1,3,2))
    Op2 = permutedims(Op2,(1,3,2))
    
    E21 = broadcast(-,transpose(E2),E1)
    
    Op = sum(Op1.*conj(Op2),dims=3)
    
    E21 = E21[:]
    Op = Op[:]
    
    # indexing for frequency
    ids = round.((log10.(abs.(E21)).+log10.(EScale).-logemin).*estep).+1
    ids[ids.<1] .= 1
    ids[ids.>nodisc] .= nodisc
    
    # indexing for sign
    sgn = Int.(E21.>=0).+1
    
    Adisc = zeros(nodisc,2);
    for i = 1:length(ids)
        Adisc[Int(ids[i]),sgn[i]] = Adisc[Int(ids[i]),sgn[i]] + Op[i];
    end
    
    return Adisc
    
    end
    