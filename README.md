# 2023 Tensor Networks Lecture - Julia Code Repository

This is the git repository for julia version of the [2023 tensor networks lecture at LMU Munich](https://moodle.lmu.de/course/view.php?id=27771).

## How to get this repository

1. Install git. You can get it from [https://git-scm.com/] for (almost) all operating systems.
2. Open a terminal. (On windows, this has to be the git terminal you just installed.)
3. Copy-paste this into the terminal and press enter:
```bash
git clone https://gitlab.physik.uni-muenchen.de/23tn/23tn-julia.git
```
4. A new folder called `23tn-julia` will be created with all material we provided.

## How to update the repository during the semester

1. Open a terminal in the `23tn-julia` folder.
2. Type the following into the terminal and press enter:
```bash
git pull
```

## How to setup Julia for the course

To solve the problem sets, you'll have to install Julia. Download and follow the installation instructions for your operating system provided at
[https://julialang.org/].

Test that you can execute Julia. Open a terminal in the `23tn-julia` folder, and type
```bash
julia
```
Something like this should appear:
```
               _
   _       _ _(_)_     |  Documentation: https://docs.julialang.org
  (_)     | (_) (_)    |
   _ _   _| |_  __ _   |  Type "?" for help, "]?" for Pkg help.
  | | | | | | |/ _` |  |
  | | |_| | | | (_| |  |  Version 1.10.5 (2024-08-27)
 _/ |\__'_|_|_|\__'_|  |  Official https://julialang.org/ release
|__/                   |

julia>
```
The version number (in my case 1.10.5) might be different. Any version >= 1.6 should be OK; if you have version 1.5 or earlier, please update your installation.

Now, type `]` into the terminal. The `julia>` prompt will switch to
```
(@v1.10) pkg>
```
or similar. You are now in "Pkg mode", which is used to download, install and update julia packages. You can get out of Pkg mode with `backspace`.

In package mode, type the following:
```
(@v1.10) pkg> add /path/23tn-julia
```
and replace `/path/` with the path to the parent folder you put `23tn-julia` into.

After typing this command and pressing enter, you should see a number of packages being downloaded and installed. This might take a while - fortunately, you only have to go through this once. Once everything has been downloaded and installed, you're ready to go! To test this, try to execute the file `problem/example/problem1.jl`:
```julia
julia> include("problem/example/problem1.jl")
```
You should see a plot that shows a gaussian function.

## VS code

I recommend using VS code as your editor. Install the julia extension, and it becomes a powerful integrated development environment. Executing, testing and debugging are made much easier with it. There is a nice guide here: https://code.visualstudio.com/docs/languages/julia

## How to structure your code

All solutions to problem sets should go into the `problem` directory; for example, there is some pre-written code for problem set 01 in `problem/set01`. Generally, it pays off to write at least 3 files:
1. **Function definitions**: A file that defines functions that solve specific parts of the problem. For more difficult tasks, grouping function definitions into multiple files (by conceptual relation) might be useful. For an example, see `problem/example/gaussian.jl`.
2. **Unit tests**: One file has tests for the functions you defined above. This can be split into multiple files as well. For example, see `problem/example/tests.jl`.
3. **Script**: One file is an executable script that calls your functions and assembles them into a solution of the whole problem. This file might also generate plots or create data files. For an example, see `problem/example/problem1.jl`.

This structure is useful, because it allows including the function definitions without executing the script. Some hints about each file:

### Function definitions file
To import the functionality provided by us, start off the file with `using tn_julia`. You might also want to import `LinearAlgebra` or other libraries, if your functions depend on their functionality.
```julia
using tn_julia
using LinearAlgebra
using JLD2

# Write function definitions here
```

### Unit tests file
To perform unit tests, you need to import `Test`, and the file that defines those functions you're going to test. The basic structure looks like this:
```julia
using Test

include("functiondefinitions.jl")

@testset "Name of testset" begin
    # Write tests here
    @test somefunction(foo) == "bar"
end

# Group your tests into multiple testsets to test multiple unrelated functions
```

### Script
Your script file has to import your function definitions, and potentially additional libraries for plotting, saving data, etc. For example:
```julia
using Plots
using LaTeXStrings
using JLD2

include("functiondefinitions.jl")

# write code here
x = -3:0.1:3
plot(x, somefunction.(x), xlabel=L"x", ylabel=L"g(x)")
```
Generally, most non-trivial code is useful in more than one place. Therefore, you should get into the habit of writing the non-trivial parts as functions, and then assemble those functions to a complete program. That way, your function can be re-used in a different place, and you don't end up solving the same problems over and over again.

### Other parts of the repository
Our pre-defined functions are in the `src/` directory, and tests are in `test/`. Later during the semester, you will find some functions and associated tests there. This would be the standard way to structure a Julia code.

As these files are sometimes updated by us to provide you with library functions, please don't put your own code there. It will cause conflicts once you try to update your repository with `git pull`.

## How to make plots

For plots, it's generally easiest to use the `Plots` library. To plot a quantity `y` against parameter `x`, simply type
```julia
using Plots
plot(x, y)
```
and a plot will appear. To get Latex plot labels, import the `LaTeXStrings` library and prepend your latex labels with `L`, like this:
```julia
using LaTeXStrings
plot(x, y, L"\gamma", L"e^{-\gamma^2}")
```

## How to hand in solutions
Put your code in the `problem/setXX/` directory. Make sure it can be executed without modifications. Put the entire `23tn-julia` directory into a zip file and upload it on Moodle.
