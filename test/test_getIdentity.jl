@testset "Test getIdentity" begin
    @testset "Identity within space" begin
        B = [
            0.0605806 0.655893 0.227513;
            0.756879 0.533726 0.000727618;;;
            0.487539 0.162312 0.974131;
            0.555496 0.341621 0.680041;;;
            0.750195 0.346508 0.155549;
            0.848631 0.740328 0.70839;;;
            0.635582 0.863085 0.627145;
            0.419916 0.240648 0.489933;;;
        ]

        @test size(B) == (2, 3, 4)

        for ind in 1:3
            A = getIdentity(B, ind)
            @test size(A, 1) == size(B, ind)
            @test size(A, 2) == size(B, ind)
            @test A == diagm(ones(size(B, ind)))
        end
    end

    @testset "Identity within product space" begin
        B = [
            0.0605806 0.655893 0.227513;
            0.756879 0.533726 0.000727618;;;
            0.487539 0.162312 0.974131;
            0.555496 0.341621 0.680041;;;
            0.750195 0.346508 0.155549;
            0.848631 0.740328 0.70839;;;
            0.635582 0.863085 0.627145;
            0.419916 0.240648 0.489933;;;
        ]
        C = [
            0.015408 0.121215 0.140853
            0.655769 0.841109 0.576761
        ]

        @test size(B) == (2, 3, 4)
        @test size(C) == (2, 3)

        for indB in 1:3, indC in 1:2
            A = getIdentity(B, indB, C, indC)
            @test size(A, 1) == size(B, indB)
            @test size(A, 2) == size(C, indC)
            @test size(A, 3) == size(B, indB) * size(C, indC)

            # Check whether A * A' is an identity in both spaces separately.
            for i in axes(A, 2)
                @test A[:, i, :] * A[:, i, :]' == diagm(ones(size(A, 1)))
            end
            for j in axes(A, 1)
                @test A[j, :, :] * A[j, :, :]' == diagm(ones(size(A, 2)))
            end
        end
    end
end
