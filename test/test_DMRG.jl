using Test
using LinearAlgebra
import tn_julia: DMRG_1site, eigs_1site

@testset "Lanczos method" begin
    L = 3   # Chain length
    # Number of Krylov steps large enough to span the whole Hilbert space
    # -> Lanczos should obtain exact ground state energy.
    N = 2^L
    E_ED = -sqrt(2) # Exact energy for a 3-site chain.

    # Create a Hamitonian for 3-site chain.
    Hleft = zeros(2, 4, 2)
    Hleft[2, 2, 1] = -1
    Hleft[1, 3, 2] = -1
    Hleft[:, 4, :] = diagm([1, 1])

    Hloc = zeros(4, 2, 4, 2)
    Hloc[1, :, 1, :] = diagm([1, 1])
    Hloc[2, 1, 1, 2] = 1
    Hloc[3, 2, 1, 1] = 1
    Hloc[4, 2, 2, 1] = -1
    Hloc[4, 1, 3, 2] = -1
    Hloc[4, :, 4, :] = diagm([1, 1])

    Hright = zeros(2, 4, 2)
    Hright[:, 1, :] = diagm([1, 1])
    Hright[1, 2, 2] = 1
    Hright[2, 3, 1] = 1

    Cinit = rand(2, 2, 2)

    # Get ground state energy.
    C, E = eigs_1site(Hleft, Hloc, Hright, Cinit; N=N)

    # Compare to exact energy.
    @test abs(E - E_ED) < 1e-14
end

@testset "One-site DMRG for small system size" begin
    L = 3   # Chain length
    E_ED = -sqrt(2) # Exact energy for a 3-site chain.

    W = [
        zeros(1, 2, 4, 2),
        zeros(4, 2, 4, 2),
        zeros(4, 2, 1, 2)
    ]

    # Create MPO for 3-site chain.
    W[1][1, 2, 2, 1] = -1
    W[1][1, 1, 3, 2] = -1
    W[1][1, :, 4, :] = diagm([1, 1])

    W[2][1, :, 1, :] = diagm([1, 1])
    W[2][2, 1, 1, 2] = 1
    W[2][3, 2, 1, 1] = 1
    W[2][4, 2, 2, 1] = -1
    W[2][4, 1, 3, 2] = -1
    W[2][4, :, 4, :] = diagm([1, 1])

    W[3][1, :, 1, :] = diagm([1, 1])
    W[3][2, 1, 1, 2] = 1
    W[3][3, 2, 1, 1] = 1

    # Random MPS as initial guess
    MPS = [
        [
            0.0111225 0.360827;;;
            0.956746 0.619604
        ],
        [
            0.314094 0.0915855; 0.200574 0.134873;;;
            0.708396 0.916657; 0.372761 0.640206;;;
            0.372158 0.473043; 0.267639 0.398434;;;
            0.297702 0.488037; 0.227023 0.203575
        ],
        [
            0.952355 0.636858; 0.892026 0.879948;
            0.968797 0.870061; 0.281807 0.559433;;;
        ]
    ]

    Nkeep = 100
    Nsweep = 100
    Econv = 1e-14

    # Get ground state energy.
    M, E0, Eiter = DMRG_1site(W, MPS, Nkeep, Nsweep, Econv=Econv)
    # Check whether it matches the exact energy.
    @test abs(E0 - E_ED) < 1e-14
end
