using tn_julia
using Test
using LinearAlgebra

include("test_getIdentity.jl")
include("test_getLocalSpace.jl")
include("test_DMRG.jl")
