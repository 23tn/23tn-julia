
@testset "Test getLocalSpace" begin
    @testset "Test getLocalSpace for spins" begin
        for s in [0.5, 1, 1.5, 3]
            localspace = getLocalSpace("Spin", s)

            @test isempty(localspace.F)
            @test isempty(localspace.Z)
            @test size(localspace.S) == (Int(2 * s + 1), 3, Int(2 * s + 1))
            @test size(localspace.I) == (Int(2 * s + 1), Int(2 * s + 1))

            Sx = (localspace.S[:, 1, :] + localspace.S[:, 2, :]) / sqrt(2)
            Sy = (localspace.S[:, 1, :] - localspace.S[:, 2, :]) / (sqrt(2) * 1im)
            Sz = localspace.S[:, 3, :]

            commutator(A, B) = A * B - B * A
            @test commutator(Sx, Sy) ≈ 1im * Sz
            @test commutator(Sy, Sz) ≈ 1im * Sx
            @test commutator(Sz, Sx) ≈ 1im * Sy

            @test localspace.I == diagm(ones(Int(2 * s + 1)))
        end
    end

    @testset "Test getLocalSpace for spinless fermions" begin
        localspace = getLocalSpace("Fermion")

        @test isempty(localspace.S)
        @test size(localspace.I) == (2, 2)
        @test size(localspace.F) == (2, 1, 2)
        @test size(localspace.Z) == (2, 2)

        anticommutator(A, B) = A * B + B * A

        @test localspace.I == [1 0; 0 1]
        @test anticommutator(localspace.F[:, 1, :]', localspace.F[:, 1, :]) == localspace.I
        @test localspace.Z * localspace.Z == localspace.I
        @test localspace.Z * localspace.F[:, 1, :] == localspace.F[:, 1, :]
        @test localspace.F[:, 1, :] * localspace.Z == -localspace.F[:, 1, :]
    end

    @testset "Test getLocalSpace for spinful fermions" begin
        localspace = getLocalSpace("FermionS")

        @test size(localspace.S) == (4, 3, 4)
        @test size(localspace.I) == (4, 4)
        @test size(localspace.F) == (4, 2, 4)
        @test size(localspace.Z) == (4, 4)

        @test localspace.I == diagm(ones(4))
        @test localspace.Z * localspace.Z == localspace.I

        anticommutator(A, B) = A * B + B * A

        for spin in [1, 2]
            @test anticommutator(localspace.F[:, spin, :]', localspace.F[:, spin, :]) == localspace.I

            # The [1, -1, -1, 1] matrices are (-1)^nparticles in the basis
            # {|nothing>, |up particle>, |down particle>, |both particles>}
            @test localspace.Z * localspace.F[:, spin, :] == diagm([1, -1, -1, 1]) * localspace.F[:, spin, :]
            @test localspace.F[:, spin, :] * localspace.Z == localspace.F[:, spin, :] * diagm([1, -1, -1, 1])
        end

        Sx = (localspace.S[:, 1, :] + localspace.S[:, 2, :]) / sqrt(2)
        Sy = (localspace.S[:, 1, :] - localspace.S[:, 2, :]) / (sqrt(2) * 1im)
        Sz = localspace.S[:, 3, :]

        commutator(A, B) = A * B - B * A
        @test commutator(Sx, Sy) ≈ 1im * Sz
        @test commutator(Sy, Sz) ≈ 1im * Sx
        @test commutator(Sz, Sx) ≈ 1im * Sy
    end
end
